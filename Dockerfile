FROM alpine:latest

RUN addgroup -g 1337 -S zeyple && adduser -u 1337 -S -G zeyple zeyple -s /bin/sh && \
    addgroup -g 2500 -S postfix && adduser -u 2500 -S -G postfix postfix

RUN apk add --no-cache --update \
       socklog s6 gnupg gpgme postfix mailx ca-certificates \
       cyrus-sasl-plain cyrus-sasl-login libsasl \
       python py-pip python3 py3-gpgme \
       python-dev python3-dev musl-dev gpgme-dev gcc swig \
    && pip install gpg tox \
    && apk del gcc python-dev python3-dev musl-dev gpgme-dev swig

RUN mkdir -p /var/mail && chown postfix:postfix /var/mail

COPY root /

EXPOSE 25

HEALTHCHECK --interval=1m --timeout=10s \
  CMD postfix status || exit 1

CMD ["/bin/sh", "/startup.sh"]
