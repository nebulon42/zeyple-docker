# zeyple-docker

This contains a Docker image and a sample Docker Compose file to use for
development purposes with Zeyple (https://github.com/infertux/zeyple).

The Docker image is also available from https://registry.gitlab.com/nebulon42/zeyple-docker/
by the name zeyple:latest.

## Usage

`docker-compose up -d` for starting and `docker stop zeypledocker_zeyple_1` to stop the container.

Move public keys that should be imported on container start into `data/keys`.
The files have to end in `.asc` to be recognized.

If you need to customize the default `docker-compose.yml` you can use a
`docker-compose.override.yml` file as described in the [Docker documentation](https://docs.docker.com/compose/extends/) 
(the override file is in `.gitignore`) and use `docker-compose up` as usual.

To send test e-mails you have to ssh into the container by issuing
`docker exec -it zeypledocker_zeyple_1 ash` (since the container is based on
the slim Alpine Linux there is no bash).

The log file of postfix can be found in `/var/log/postfix/current` and Zeyple's
log file is in `/var/log/zeyple.log`.
