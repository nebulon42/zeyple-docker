#!/bin/sh

set -e # terminate on errors
set -xo pipefail

KEYDIR=/var/lib/zeyple/keys
ZEYPLE_LOGFILE=/var/log/zeyple.log
POSTFIX_LOGDIR=/var/log/postfix
POSTFIX_CONFIGDIR=/etc/postfix
S6_DIR=/etc/s6

if [ ! -d "$KEYDIR" ]; then
    # do setup tasks
    touch $ZEYPLE_LOGFILE
    chown zeyple:zeyple $ZEYPLE_LOGFILE

    mkdir -p $POSTFIX_LOGDIR
    chown postfix:postfix $POSTFIX_LOGDIR

    chown -R postfix:postfix $POSTFIX_CONFIGDIR
    chown root:postfix $POSTFIX_CONFIGDIR/dynamicmaps.cf

    mkdir -p $KEYDIR
    chmod 700 $KEYDIR
    chown zeyple:zeyple $KEYDIR

    # configure hostname and domain for postfix
    if [ ! -z "$POSTFIX_MYHOSTNAME" ]; then
        echo "myhostname=$POSTFIX_MYHOSTNAME" >> $POSTFIX_CONFIGDIR/main.cf
    else
        echo 'myhostname=localhost.local' >> $POSTFIX_CONFIGDIR/main.cf
    fi

    if [ ! -z "$POSTFIX_MYDOMAIN" ]; then
        echo "mydomain=$POSTFIX_MYDOMAIN" >> $POSTFIX_CONFIGDIR/main.cf
    else
        echo 'mydomain=localhost.local' >> $POSTFIX_CONFIGDIR/main.cf
    fi

    # configure relay
    if [ ! -z "$POSTFIX_RELAYHOST" ]; then
        echo "relayhost=$POSTFIX_RELAYHOST" >> $POSTFIX_CONFIGDIR/main.cf

        if [ ! -z "$RELAY_AUTH" ] && [ "$RELAY_AUTH" = "true" ]; then
            echo 'smtp_sasl_auth_enable = yes' >> $POSTFIX_CONFIGDIR/main.cf
            echo 'smtp_sasl_security_options = noanonymous' >> $POSTFIX_CONFIGDIR/main.cf

            if [ -f "/etc/postfix/sasl_passwd" ]; then
                postmap $POSTFIX_CONFIGDIR/sasl_passwd
                echo "smtp_sasl_password_maps = hash:$POSTFIX_CONFIGDIR/sasl_passwd" >> $POSTFIX_CONFIGDIR/main.cf
            fi
        fi

        if [ ! -z "$RELAY_TLS" ] && [ "$RELAY_TLS" = "true" ]; then
            echo 'smtp_use_tls = yes' >> $POSTFIX_CONFIGDIR/main.cf
            echo 'smtp_tls_security_level = encrypt' >> $POSTFIX_CONFIGDIR/main.cf
            echo 'smtp_tls_wrappermode = yes' >> $POSTFIX_CONFIGDIR/main.cf
            echo 'smtp_sasl_tls_security_options = noanonymous' >> $POSTFIX_CONFIGDIR/main.cf
        fi
    fi

    # import public keys
    if [ ! -z "$KEY_IMPORT_DIR" ]; then
        cd $KEY_IMPORT_DIR
        for f in *.asc
        do
            su - zeyple -c "cd $KEY_IMPORT_DIR && gpg --homedir $KEYDIR --import $f"
        done
    fi
fi

exec /bin/s6-svscan $S6_DIR
